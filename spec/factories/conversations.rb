FactoryGirl.define do
  factory :conversation, class: Conversation do
    association :ticket
  end
end
