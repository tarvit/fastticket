FactoryGirl.define do
  factory :message, class: Message do
    association :conversation
    author_name { Faker::Name.name }
    text { Faker::Lorem.sentence }
  end
end
