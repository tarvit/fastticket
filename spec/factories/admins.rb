FactoryGirl.define do
  factory :admin, class: Admin do
    email { Faker::Internet.email }
    password { 'qweasd00' }
  end
end
