FactoryGirl.define do
  factory :client, class: Client do
    email { Faker::Internet.email }
    name { Faker::Name.name }
    password { 'qweasd00' }
  end
end
