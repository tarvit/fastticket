FactoryGirl.define do
  factory :ticket, class: Ticket do
    status { Ticket::STATUSES.first }

    description { Faker::Lorem.paragraphs.join }
    title { Faker::Lorem.sentence }

    association :agent
    association :client
  end
end
