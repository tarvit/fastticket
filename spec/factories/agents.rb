FactoryGirl.define do
  factory :agent, class: Agent do
    email { Faker::Internet.email }
    name { Faker::Name.name }
    password { 'qweasd00' }
  end
end
