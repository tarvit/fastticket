require 'rails_helper'

load 'features/support/pages/actions/base.rb'
Dir.glob(File.join('spec/features/support', '**', '*.rb'), &method(:load))

RSpec.configure do |config|
  config.use_transactional_fixtures = false

  DatabaseCleaner.strategy = :truncation

  config.before(:each) { DatabaseCleaner.clean }
  config.after(:each) { DatabaseCleaner.clean }

  Capybara.register_driver :selenium do |app|
    Capybara::Selenium::Driver.new(app, browser: :chrome)
  end

  Capybara.default_driver = :selenium

  Swat::Capybara.setup(config, { default_pause: 0.2,
                                 min_pause: 0.1,
                                 tries: 10,
                                 default_selector: 'body',
                                 output: { enabled: true, started: ?>, step: ?. }
  })
end
