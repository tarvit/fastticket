require 'rails_helper'

describe Api::Clients::TicketsController, type: 'controller' do
  describe '#create' do
    let(:request) do
      ticket = { title: 'target', description: '?', category: 'Information' }
      post 'create', params: { ticket: ticket }
    end

    context 'when not authorized' do
      subject { request }

      it { is_expected.to have_http_status :unauthorized }
    end

    context 'when authorized' do
      let!(:client) { create(:client) }

      before { sign_in client }

      subject { request }

      it { is_expected.to have_http_status :success }

      it do
        subject
        expect(response.body).to include 'result', 'true'
      end

      context 'when invalid request' do
        let(:request) { post 'create', params: {} }

        it { is_expected.to have_http_status :internal_server_error }
      end
    end
  end
end
