require 'rails_helper'

describe Api::Clients::TicketsController, type: 'controller' do
  describe '#index' do
    context 'when not authorized' do
      subject { get 'index' }

      it { is_expected.to have_http_status :unauthorized }
    end

    context 'when authorized' do
      let!(:client) { create(:client) }

      before { sign_in client }

      subject { get 'index' }

      it { is_expected.to have_http_status :success }

      it do
        subject
        expect(response.body).to include 'tickets', '[]'
      end

      context 'when ticket exists' do
        let!(:ticket) { create(:ticket, title: 'target', client: client) }

        it { is_expected.to have_http_status :success }

        it do
          subject
          expect(response.body).to include 'tickets', 'target'
        end

        context 'when ticket belongs to other client' do
          let!(:ticket) { create(:ticket, title: 'target', client: create(:client)) }

          it { is_expected.to have_http_status :success }

          it do
            subject
            expect(response.body).to include 'tickets'
            expect(response.body).to_not include 'target'
          end
        end
      end
    end
  end
end
