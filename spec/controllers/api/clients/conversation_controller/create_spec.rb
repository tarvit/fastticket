require 'rails_helper'

describe Api::Clients::ConversationsController, type: 'controller' do
  describe('#create') do
    let(:ticket) { double(id: 0) }

    let(:request) { post 'create', params: { id: ticket.id, text: 'target' } }

    context 'when not authorized' do
      subject { request }

      it { is_expected.to have_http_status :unauthorized }
    end

    context 'when authorized' do
      let!(:client) { create(:client) }
      let!(:ticket) do
        create(:ticket, client: client, status: Ticket::PROCESSING,
               conversation: create(:conversation))
      end
      let!(:message) { create(:message, text: 'target', conversation: ticket.conversation) }

      before { sign_in client }

      subject { request }

      it { is_expected.to have_http_status :created }

      it do
        subject
        expect(response.body).to include 'result', 'true'
      end

      context 'when ticket has closed status' do
        let!(:ticket) do
          create(:ticket, title: 'target', client: client,
                 status: Ticket::CLOSED, conversation: create(:conversation))
        end

        subject { request }

        it { is_expected.to have_http_status :internal_server_error }
      end

      context 'when ticket belongs to other client' do
        let!(:ticket) do
          create(:ticket, title: 'target', client: create(:client),
                 status: Ticket::PROCESSING, conversation: create(:conversation))
        end

        subject { request }

        it { is_expected.to have_http_status :internal_server_error }
      end
    end
  end
end
