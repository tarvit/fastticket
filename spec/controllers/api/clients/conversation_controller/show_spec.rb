require 'rails_helper'

describe Api::Clients::ConversationsController, type: 'controller' do
  describe('#show') do
    let(:ticket) { double(id: 0) }

    context 'when not authorized' do
      subject { get 'show', params: { id: ticket.id } }

      it { is_expected.to have_http_status :unauthorized }
    end

    context 'when authorized' do
      let!(:client) { create(:client) }
      let!(:ticket) do
        create(:ticket, client: client, status: Ticket::PROCESSING,
                        conversation: create(:conversation))
      end
      let!(:message) { create(:message, text: 'target', conversation: ticket.conversation) }

      before { sign_in client }

      subject { get 'show', params: { id: ticket.id } }

      it { is_expected.to have_http_status :success }

      it do
        subject
        expect(response.body).to include 'messages', 'target'
      end

      context 'when ticket has closed status' do
        let!(:ticket) do
          create(:ticket, title: 'target', client: client,
                          status: Ticket::CLOSED, conversation: create(:conversation))
        end

        it { is_expected.to have_http_status :success }

        it do
          subject
          expect(response.body).to include 'messages', 'target'
        end
      end

      context 'when ticket belongs to other client' do
        let!(:ticket) do
          create(:ticket, title: 'target', client: create(:client),
                          status: Ticket::PROCESSING, conversation: create(:conversation))
        end

        it { is_expected.to have_http_status :internal_server_error }
      end
    end
  end
end
