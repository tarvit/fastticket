require 'rails_helper'

describe Api::Agents::TicketsController, type: 'controller' do
  describe '#close' do
    let(:ticket) { double(id: 0) }

    context 'when not authorized' do
      subject { patch 'close', params: { id: ticket.id } }

      it { is_expected.to have_http_status :unauthorized }
    end

    context 'when authorized' do
      context 'when not verified' do
        let!(:agent) { create(:agent) }

        before { sign_in agent }

        subject { patch 'close', params: { id: ticket.id } }

        it { is_expected.to have_http_status :unauthorized }
      end

      context 'when verified' do
        let!(:agent) { create(:agent, verified: true) }
        let!(:ticket) do
          create(:ticket, title: 'target', agent: agent, status: Ticket::PROCESSING)
        end

        before { sign_in agent }

        subject { patch 'close', params: { id: ticket.id } }

        it { is_expected.to have_http_status :success }

        context 'when ticket has invalid status' do
          let!(:ticket) do
            create(:ticket, title: 'target', agent: nil, status: Ticket::CLOSED)
          end

          it { is_expected.to have_http_status :internal_server_error }
        end

        context 'when ticket belongs to other agent' do
          let!(:ticket) do
            create(:ticket, title: 'target', agent: create(:agent), status: Ticket::PROCESSING)
          end

          it { is_expected.to have_http_status :internal_server_error }
        end
      end
    end
  end
end
