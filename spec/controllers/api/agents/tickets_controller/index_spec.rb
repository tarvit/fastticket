require 'rails_helper'

describe Api::Agents::TicketsController, type: 'controller' do
  describe '#index' do
    context 'when not authorized' do
      subject { get 'index' }

      it { is_expected.to have_http_status :unauthorized }
    end

    context 'when authorized' do
      context 'when not verified' do
        let!(:agent) { create(:agent) }

        before { sign_in agent }

        subject { get 'index' }

        it { is_expected.to have_http_status :unauthorized }
      end

      context 'when verified' do
        let!(:agent) { create(:agent, verified: true) }

        before { sign_in agent }

        subject { get 'index' }

        it { is_expected.to have_http_status :success }

        it do
          subject
          expect(response.body).to include 'tickets', '[]'
        end

        context 'when ticket exists' do
          let!(:ticket) { create(:ticket, title: 'target') }

          it { is_expected.to have_http_status :success }

          it do
            subject
            expect(response.body).to include 'tickets', 'target'
          end
        end
      end
    end
  end
end
