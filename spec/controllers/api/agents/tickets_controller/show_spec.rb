require 'rails_helper'

describe Api::Agents::TicketsController, type: 'controller' do
  describe '#show' do
    let(:ticket) { double(id: 0) }

    context 'when not authorized' do
      subject { get 'show', params: { id: ticket.id } }

      it { is_expected.to have_http_status :unauthorized }
    end

    context 'when authorized' do
      context 'when not verified' do
        let!(:agent) { create(:agent) }

        before { sign_in agent }

        subject { get 'show', params: { id: ticket.id } }

        it { is_expected.to have_http_status :unauthorized }
      end

      context 'when verified' do
        let!(:agent) { create(:agent, verified: true) }
        let!(:ticket) { create(:ticket, title: 'target') }

        before { sign_in agent }

        subject { get 'show', params: { id: ticket.id } }

        it { is_expected.to have_http_status :success }

        it do
          subject
          expect(response.body).to include 'ticket', 'target'
        end
      end
    end
  end
end
