require 'rails_helper'

describe Api::Agents::TicketsController, type: 'controller' do
  describe '#process_ticket' do
    let(:ticket) { double(id: 0) }

    context 'when not authorized' do
      subject { patch 'process_ticket', params: { id: ticket.id } }

      it { is_expected.to have_http_status :unauthorized }
    end

    context 'when authorized' do
      context 'when not verified' do
        let!(:agent) { create(:agent) }

        before { sign_in agent }

        subject { patch 'process_ticket', params: { id: ticket.id } }

        it { is_expected.to have_http_status :unauthorized }
      end

      context 'when verified' do
        let!(:agent) { create(:agent, verified: true) }
        let!(:ticket) { create(:ticket, title: 'target', agent: nil) }

        before { sign_in agent }

        subject { patch 'process_ticket', params: { id: ticket.id } }

        it { is_expected.to have_http_status :success }

        context 'when ticket has invalid status' do
          let!(:ticket) do
            create(:ticket, title: 'target', agent: nil, status: Ticket::CLOSED)
          end

          it { is_expected.to have_http_status :internal_server_error }
        end

        context 'when ticket belongs to other agent' do
          let!(:ticket) { create(:ticket, title: 'target', agent: create(:agent)) }

          it { is_expected.to have_http_status :internal_server_error }
        end
      end
    end
  end
end
