require 'rails_helper'

describe Api::Agents::ConversationsController, type: 'controller' do
  describe('#show') do
    let(:ticket) { double(id: 0) }

    context 'when not authorized' do
      subject { get 'show', params: { id: ticket.id } }

      it { is_expected.to have_http_status :unauthorized }
    end

    context 'when authorized' do
      context 'when not verified' do
        let!(:agent) { create(:agent) }

        before { sign_in agent }

        subject { get 'show', params: { id: ticket.id } }

        it { is_expected.to have_http_status :unauthorized }
      end

      context 'when verified' do
        let!(:agent) { create(:agent, verified: true) }
        let!(:ticket) do
          create(:ticket, agent: agent, status: Ticket::PROCESSING,
                          conversation: create(:conversation))
        end
        let!(:message) { create(:message, text: 'target', conversation: ticket.conversation) }

        before { sign_in agent }

        subject { get 'show', params: { id: ticket.id } }

        it { is_expected.to have_http_status :success }

        it do
          subject
          expect(response.body).to include 'messages', 'target'
        end

        context 'when ticket has closed status' do
          let!(:ticket) do
            create(:ticket, title: 'target', agent: nil,
                            status: Ticket::CLOSED, conversation: create(:conversation))
          end

          it { is_expected.to have_http_status :success }

          it do
            subject
            expect(response.body).to include 'messages', 'target'
          end
        end

        context 'when ticket belongs to other agent' do
          let!(:ticket) do
            create(:ticket, title: 'target', agent: create(:agent),
                            status: Ticket::PROCESSING, conversation: create(:conversation))
          end

          it { is_expected.to have_http_status :success }

          it do
            subject
            expect(response.body).to include 'messages', 'target'
          end
        end
      end
    end
  end
end
