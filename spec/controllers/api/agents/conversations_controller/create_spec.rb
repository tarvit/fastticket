require 'rails_helper'

describe Api::Agents::ConversationsController, type: 'controller' do
  describe('#create') do
    let(:ticket) { double(id: 0) }

    let(:request) { post 'create', params: { id: ticket.id, text: 'target' } }

    context 'when not authorized' do
      subject { request }

      it { is_expected.to have_http_status :unauthorized }
    end

    context 'when authorized' do
      context 'when not verified' do
        let!(:agent) { create(:agent) }

        before { sign_in agent }

        subject { request }

        it { is_expected.to have_http_status :unauthorized }
      end

      context 'when verified' do
        let!(:agent) { create(:agent, verified: true) }
        let!(:ticket) do
          create(:ticket, agent: agent, status: Ticket::PROCESSING,
                 conversation: create(:conversation))
        end
        let!(:message) { create(:message, text: 'target', conversation: ticket.conversation) }

        before { sign_in agent }

        subject { request }

        it { is_expected.to have_http_status :created }

        it do
          subject
          expect(response.body).to include 'result', 'true'
        end

        context 'when ticket has closed status' do
          let!(:ticket) do
            create(:ticket, title: 'target', agent: agent,
                   status: Ticket::CLOSED, conversation: create(:conversation))
          end

          subject { request }

          it { is_expected.to have_http_status :internal_server_error }
        end

        context 'when ticket belongs to other agent' do
          let!(:ticket) do
            create(:ticket, title: 'target', agent: create(:agent),
                   status: Ticket::PROCESSING, conversation: create(:conversation))
          end

          subject { request }

          it { is_expected.to have_http_status :internal_server_error }
        end
      end
    end
  end
end
