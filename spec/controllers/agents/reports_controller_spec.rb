require 'rails_helper'

describe Agents::ReportsController, type: 'controller' do
  context 'when not authorized' do
    subject { get 'index' }

    it { is_expected.to redirect_to new_agent_session_path }
  end

  context 'when authorized' do
    context 'when not verified' do
      let(:agent) { create(:agent) }

      before { sign_in agent }

      subject { get 'index' }

      it { is_expected.to redirect_to agent_not_verified_path }
    end

    context 'when verified' do
      let(:agent) { create(:agent, verified: true) }

      before { sign_in agent }

      subject { get 'index' }

      it { is_expected.to render_template :index }
    end
  end
end
