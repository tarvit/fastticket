require 'rails_helper'

describe Clients::DashboardController, type: 'controller' do
  context 'when not authorized' do
    subject { get 'index' }

    it { is_expected.to redirect_to new_client_session_path }
  end

  context 'when authorized' do
    let(:client) { create(:client) }

    before { sign_in client }

    subject { get 'index' }

    it { is_expected.to render_template :index }
  end
end
