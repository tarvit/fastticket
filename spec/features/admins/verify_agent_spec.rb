require 'capybara_helper'

describe 'Admin verifies agent', type: :feature do
  let(:email) { 'some@em.ail' }
  let(:password) { 'simple1' }

  let!(:admin) { create(:admin, email: email, password: password) }
  let!(:agent) { create(:agent, verified: false) }

  it do
    Pages::Actions::Admins::LogIn.new(email, password).act
    Pages::Actions::Admins::VerifyAgent.new.act

    expect(page.text).to include('Agent', 'updated')
    expect(agent.reload.verified).to be_truthy
  end
end
