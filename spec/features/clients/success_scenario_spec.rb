require 'capybara_helper'

describe 'Clients Success Scenario', type: :feature do
  let(:email) { 'some@em.ail' }
  let(:name) { 'John Doe' }
  let(:password) { 'simple1' }

  let(:category) { 'Technical' }
  let(:description) { 'Black Screen' }
  let(:title) { 'TV does not work' }

  let(:message) { 'Hello!' }

  it do
    Pages::Actions::Clients::SignUp.new(email, password, name).act

    check_text 'My Tickets', '.panel-heading'

    Pages::Actions::Clients::SubmitTicket.new(
        title, description, category
    ).act

    check_text 'submitted successfully', 'h1'

    Pages::Actions::Clients::PostMessage.new(message).act

    check_text message, '.ft-message .text'
  end
end
