require 'capybara_helper'

describe 'Client sign up', type: :feature do
  let(:email) { 'some@em.ail' }
  let(:name) { 'John Doe' }
  let(:password) { 'simple1' }

  it do
    Pages::Actions::Clients::SignUp.new(email, password, name).act

    expect(find('.panel-heading').text).to eq 'My Tickets'
  end
end
