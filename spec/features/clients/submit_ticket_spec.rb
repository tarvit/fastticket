require 'capybara_helper'

describe 'Clients Success Scenario', type: :feature do
  let(:password) { 'simple1' }
  let!(:client) { create(:client, password: password) }

  let(:category) { 'Technical' }
  let(:description) { 'Black Screen' }
  let(:title) { 'TV does not work' }

  let(:message) { 'Hello!' }

  it do
    Pages::Actions::Clients::LogIn.new(client.email, password).act

    check_text 'My Tickets', '.panel-heading'

    Pages::Actions::Clients::SubmitTicket.new(
        title, description, category
    ).act

    check_text 'submitted successfully', 'h1'
  end
end
