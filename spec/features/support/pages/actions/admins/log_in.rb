module Pages
  module Actions
    module Admins
      class LogIn < Pages::Actions::Base

        def initialize(email, password)
          @email = email
          @password = password
        end

        def act
          visit '/superadmin'

          safe_find('#admin_email').set @email
          safe_find('#admin_password').set @password

          safe_click '#login_btn'
        end
      end
    end
  end
end
