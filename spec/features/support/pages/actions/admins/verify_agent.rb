module Pages
  module Actions
    module Admins
      class VerifyAgent < Pages::Actions::Base

        def act
          visit '/superadmin'

          safe_find('.sidebar-nav').click_link 'Agents'

          safe_click('.icon-pencil')

          safe_find('#agent_verified').set(true)

          click_by_text 'Save', 'button.btn-primary'
        end
      end
    end
  end
end
