module Pages
  module Actions
    module Agents
      class LogIn < Pages::Actions::Base
        def initialize(email, password)
          @email = email
          @password = password
        end

        def act
          visit '/agents'

          safe_find('#agent_email').set @email
          safe_find('#agent_password').set @password

          safe_click '#login_btn'
        end
      end
    end
  end
end
