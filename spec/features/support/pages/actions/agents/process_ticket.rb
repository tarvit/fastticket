module Pages
  module Actions
    module Agents
      class ProcessTicket < Pages::Actions::Base
        def act
          visit '/agents'

          safe_find('.ft-process-btn').click
        end
      end
    end
  end
end
