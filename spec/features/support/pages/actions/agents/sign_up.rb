module Pages
  module Actions
    module Agents
      class SignUp < Pages::Actions::Base
        def initialize(email, password, name)
          @email = email
          @password = password
          @name = name
        end

        def act
          visit '/agents'

          click_by_text 'Sign up', 'a'

          safe_find('#agent_email').set @email
          safe_find('#agent_name').set @name
          safe_find('#agent_password').set @password
          safe_find('#agent_password_confirmation').set @password

          safe_click('#sign_up_btn')

          check_text 'Try Again'

          Agent.where(email: @email).take.update_attribute(:verified, true)

          click_by_text 'Try Again', 'a'
        end
      end
    end
  end
end
