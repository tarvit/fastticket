module Pages
  module Actions
    module Agents
      class Filtering < Pages::Actions::Base

        def reset
          set_agent('Any')
          set_status('Any')
        end

        def show_mine
          set_agent('Mine')
        end

        def show_new
          set_status('new')
        end

        def show_closed
          set_status('closed')
        end

        private

        def set_agent(value)
          safe_find('#agent').select(value)
        end

        def set_status(value)
          safe_find('#status').select(value)
        end
      end
    end
  end
end
