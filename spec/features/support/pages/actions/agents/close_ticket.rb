module Pages
  module Actions
    module Agents
      class CloseTicket < Pages::Actions::Base

        def act
          visit '/agents'

          safe_find('.ft-view-btn').click

          safe_find('.ft-close-btn').click
        end
      end
    end
  end
end
