module Pages
  module Actions
    module Agents
      class PostMessage < Pages::Actions::Base
        def initialize(message)
          @message = message
        end

        def act
          visit '/agents'

          safe_find('.ft-view-btn').click

          safe_find('.ft-post-message input').set(@message)
          safe_find('.ft-post-message button').click
        end
      end
    end
  end
end
