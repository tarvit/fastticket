module Pages
  module Actions
    class Base
      include Capybara::DSL
      include Swat::Capybara::PrintHelper
      include Swat::Capybara::Helpers
    end
  end
end
