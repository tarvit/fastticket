module Pages
  module Actions
    module Clients
      class SubmitTicket < Pages::Actions::Base

        def initialize(title, description, category)
          @title = title
          @description = description
          @category = category
        end

        def act
          visit '/'

          click_by_text 'Submit a Ticket', 'button'

          safe_find('form #title').set @title
          safe_find('form #description').set @description
          safe_find('form #category').select @category

          click_by_text 'Submit', 'button'
        end
      end
    end
  end
end
