module Pages
  module Actions
    module Clients
      class SignUp < Pages::Actions::Base

        def initialize(email, password, name)
          @email = email
          @password = password
          @name = name
        end

        def act
          visit '/'

          click_by_text 'Sign up', 'a'

          safe_find('#client_email').set @email
          safe_find('#client_name').set @name
          safe_find('#client_password').set @password
          safe_find('#client_password_confirmation').set @password

          safe_click('#sign_up_btn')
        end
      end
    end
  end
end
