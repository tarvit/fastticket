module Pages
  module Actions
    module Clients
      class LogIn < Pages::Actions::Base
        def initialize(email, password)
          @email = email
          @password = password
        end

        def act
          visit '/'

          safe_find('#client_email').set @email
          safe_find('#client_password').set @password

          safe_click '#login_btn'
        end
      end
    end
  end
end
