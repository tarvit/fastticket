require 'capybara_helper'

describe 'Agent processes a ticket', type: :feature do
  let!(:agent) do
    create(:agent, email: email, password: password, verified: true)
  end

  let!(:tickets) do
    [
        create(:ticket, title: 'target', agent: nil, status: Ticket::NEW),
        create(:ticket, status: Ticket::NEW),
        create(:ticket, status: Ticket::CLOSED),
        create(:ticket, status: Ticket::CLOSED, agent: agent)
    ]
  end

  let(:email) { 'some@em.ail' }
  let(:password) { 'simple1' }

  it do
    Pages::Actions::Agents::LogIn.new(email, password).act
    Pages::Actions::Agents::ProcessTicket.new.act
  end
end
