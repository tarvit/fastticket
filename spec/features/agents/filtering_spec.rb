require 'capybara_helper'

describe 'Agent processes a ticket', type: :feature do
  let!(:agent) do
    create(:agent, email: email, password: password, verified: true)
  end

  let!(:tickets) do
    [
        create(:ticket, title: 'targetA', agent: nil, status: Ticket::NEW),
        create(:ticket, title: 'targetB', status: Ticket::NEW),
        create(:ticket, title: 'targetC', status: Ticket::CLOSED),
        create(:ticket, title: 'targetD', status: Ticket::CLOSED, agent: agent)
    ]
  end

  let(:email) { 'some@em.ail' }
  let(:password) { 'simple1' }

  it do
    Pages::Actions::Agents::LogIn.new(email, password).act

    filtering = Pages::Actions::Agents::Filtering.new
    filtering.reset

    check_text('targetA')
    check_text('targetB')
    check_text('targetC')
    check_text('targetD')

    filtering.show_new

    check_text('targetA')
    check_text('targetB')
    check_no_text('targetC')
    check_no_text('targetD')

    filtering.show_mine

    check_no_text('targetA')
    check_no_text('targetB')
    check_no_text('targetC')
    check_no_text('targetD')

    filtering.reset
    filtering.show_closed

    check_no_text('targetA')
    check_no_text('targetB')
    check_text('targetC')
    check_text('targetD')
  end
end
