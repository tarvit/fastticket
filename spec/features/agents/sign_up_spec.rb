require 'capybara_helper'

describe 'Agent signs up', type: :feature do
  let(:email) { 'some@em.ail' }
  let(:name) { 'John Doe' }
  let(:password) { 'simple1' }

  it do
    Pages::Actions::Agents::SignUp.new(email, password, name).act

    expect(safe_find('.panel-heading').text).to eq 'Tickets'
  end
end
