require 'capybara_helper'

describe 'Agent logs in', type: :feature do
  let!(:agent) do
    create(:agent, email: email, password: password, verified: true)
  end

  let(:email) { 'some@em.ail' }
  let(:password) { 'simple1' }

  it do
    Pages::Actions::Agents::LogIn.new(email, password).act

    expect(safe_find('.panel-heading').text).to eq 'Tickets'
  end
end
