require 'capybara_helper'

describe 'Agent posts a message', type: :feature do
  let!(:agent) do
    create(:agent, email: email, password: password, verified: true)
  end

  let!(:ticket) do
    ticket = create(:ticket, title: 'target', agent: agent, status: Ticket::PROCESSING)
    create(:conversation, ticket: ticket)
    ticket
  end

  let(:message) { 'I am here to help you' }
  let(:email) { 'some@em.ail' }
  let(:password) { 'simple1' }

  it do
    Pages::Actions::Agents::LogIn.new(email, password).act

    Pages::Actions::Agents::CloseTicket.new.act

    visit '/agents'

    check_text 'Closed'
  end
end
