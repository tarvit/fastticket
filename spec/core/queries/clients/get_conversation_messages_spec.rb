require 'rails_helper'

describe FastTicket::Queries::Clients::GetConversationMessages do
  let!(:ticket) { create(:ticket) }
  let!(:client) { ticket.client }
  let!(:conversation) { create(:conversation, ticket: ticket) }
  let!(:message) { create(:message, conversation: conversation) }

  let(:command) do
    described_class.new(client_id: client.id, ticket_id: ticket.id)
  end

  subject { command.call }

  let(:result) do
    {
        messages: [
            {
                id: message.id,
                text: message.text,
                author_name: message.author_name,
                conversation_id: conversation.id,
                created_at: kind_of(String),
                timestamp: kind_of(Integer),
            }
        ]
    }
  end

  it { is_expected.to match result }

  context 'when no messages' do
    let!(:message) { nil }

    it { is_expected.to eq({ messages: []}) }
  end

  context 'when other client' do
    let!(:client) { create(:client) }

    it do
      expect { subject }.to raise_error(FastTicket::Base::Error)
    end
  end
end
