require 'rails_helper'

describe FastTicket::Queries::Clients::GetTickets do
  let!(:client) { create(:client) }
  let!(:ticket) { create(:ticket, client: client) }

  let(:command) do
    described_class.new(client_id: client.id)
  end

  subject { command.call }

  let(:result) do
    [
        {
            id: ticket.id,
            title: ticket.title,
            created_at: kind_of(String),
            category: nil,
            description: ticket.description,
            requester: { name: ticket.client.name },
            support_agent: { name: ticket.agent.name, id: ticket.agent.id },
            status: { value: 'new', label: 'New' }
        }
    ]
  end

  it { is_expected.to match result }

  context 'when no tickets' do
    let!(:ticket) { nil }

    it { is_expected.to eq [] }
  end
end
