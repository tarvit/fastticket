require 'rails_helper'

describe FastTicket::Queries::Clients::GetTicket do
  let!(:ticket) { create(:ticket) }
  let!(:client) { ticket.client }

  let(:command) do
    described_class.new(client_id: client.id, ticket_id: ticket.id)
  end

  subject { command.call }

  let(:result) do
    {
        id: ticket.id,
        title: ticket.title,
        created_at: kind_of(String),
        category: nil,
        description: ticket.description,
        requester: { name: ticket.client.name },
        support_agent: { name: ticket.agent.name, id: ticket.agent.id },
        status: { value: 'new', label: 'New' }
    }
  end

  it { is_expected.to match result }

  context 'when other client' do
    let!(:client) { create(:client) }

    it { is_expected.to be_nil }
  end
end
