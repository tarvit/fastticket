require 'rails_helper'

describe FastTicket::Queries::Agents::GetTickets do
  before { Timecop.freeze(Time.local(2017, 9, 15, 10, 5, 0)) }
  after { Timecop.return }

  let!(:agent) { create(:agent) }

  let!(:tickets) do
    [
        create(:ticket, title: '1', status: Ticket::NEW, category: 'Info',
                        agent: agent, updated_at: DateTime.now),

        create(:ticket, title: '2', status: Ticket::PROCESSING, category: 'Info',
                        agent: nil, updated_at: (DateTime.now - 3.days)),

        create(:ticket, title: '3', status: Ticket::PROCESSING, category: 'Other',
                        agent: nil, updated_at: (DateTime.now - 10.days)),
    ]
  end

  let(:filters) { {} }

  let(:command) do
    described_class.new(agent_id: agent.id, filters: filters)
  end

  subject do
    command.call.map {|r| r[:title] }
  end

  describe 'no filters' do
    it { is_expected.to match_array %w(1 2 3) }
  end

  describe 'status' do
    context 'when any' do
      let(:filters) { {status: 'any'} }

      it { is_expected.to match_array %w(1 2 3) }
    end

    context 'when new' do
      let(:filters) { {status: 'new'} }

      it { is_expected.to match_array %w(1) }
    end

    context 'when processing' do
      let(:filters) { {status: 'processing'} }

      it { is_expected.to match_array %w(2 3) }
    end

    context 'when invalid' do
      let(:filters) { {status: 'invalid'} }

      it { is_expected.to eq [] }
    end
  end

  describe 'agent' do
    context 'when any' do
      let(:filters) { {agent: 'any'} }

      it { is_expected.to match_array %w(1 2 3) }
    end

    context 'when not_assigned' do
      let(:filters) { {agent: 'not_assigned'} }

      it { is_expected.to match_array %w(2 3) }
    end

    context 'when processing' do
      let(:filters) { {agent: 'mine'} }

      it { is_expected.to match_array %w(1) }
    end
  end

  describe 'category' do
    context 'when any' do
      let(:filters) { {category: 'any'} }

      it { is_expected.to match_array %w(1 2 3) }
    end

    context 'when Info' do
      let(:filters) { {category: 'Info'} }

      it { is_expected.to match_array %w(1 2) }
    end

    context 'when Other' do
      let(:filters) { {category: 'Other'} }

      it { is_expected.to match_array %w(3) }
    end

    context 'when Strange' do
      let(:filters) { {category: 'Strange'} }

      it { is_expected.to eq [] }
    end
  end

  describe 'period' do
    context 'when any' do
      let(:filters) { {period: 'any'} }

      it { is_expected.to match_array %w(1 2 3) }
    end

    context 'when Info' do
      let(:filters) { {period: 'today'} }

      it { is_expected.to match_array %w(1) }
    end

    context 'when Other' do
      let(:filters) { {period: 'last_week'} }

      it { is_expected.to match_array %w(1 2) }
    end

    context 'when Strange' do
      let(:filters) { {period: 'last_month'} }

      it { is_expected.to match_array %w(1 2 3) }
    end
  end
end
