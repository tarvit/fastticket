require 'rails_helper'

describe FastTicket::Queries::Agents::GetTickets do
  let!(:tickets) { [create(:ticket)] }
  let!(:agent) { create(:agent) }
  let(:filters) { {} }

  let(:command) do
    described_class.new(agent_id: agent.id, filters: filters)
  end

  subject { command.call }

  let(:result) do
    [
        {
            id: tickets[0].id,
            title: tickets[0].title,
            created_at: kind_of(String),
            category: nil,
            description: tickets[0].description,
            requester: { name: tickets[0].client.name },
            support_agent: { name: tickets[0].agent.name, id: tickets[0].agent.id },
            status: { value: 'new', label: 'New' }
        }
    ]
  end

  it { is_expected.to match result }

  context 'when no tickets' do
    let!(:tickets) { nil }

    it { is_expected.to eq [] }
  end
end
