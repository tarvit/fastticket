require 'rails_helper'

describe FastTicket::Queries::Agents::GetTicket do
  let!(:ticket) { create(:ticket) }

  let(:command) do
    described_class.new(ticket_id: ticket.id)
  end

  subject { command.call }

  let(:result) do
    {
        id: ticket.id,
        title: ticket.title,
        created_at: kind_of(String),
        category: nil,
        description: ticket.description,
        requester: { name: ticket.client.name },
        support_agent: { name: ticket.agent.name, id: ticket.agent.id },
        status: { value: 'new', label: 'New' }
    }
  end

  it { is_expected.to match result }

  context 'when no ticket' do
    let!(:ticket) { double(id: nil) }

    it do
      expect{ subject }.to raise_error(FastTicket::Base::Error)
    end
  end
end
