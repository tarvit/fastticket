require 'rails_helper'

describe FastTicket::Commands::Agents::PostMessage do
  let!(:agent) { create(:agent) }
  let!(:ticket) { create(:ticket, status: Ticket::PROCESSING, agent: agent) }
  let!(:conversation) { create(:conversation, ticket: ticket) }

  let(:command) do
    described_class.new(ticket_id: ticket.id, agent_id: agent.id, text: 'my text')
  end

  subject { command.call }

  it { is_expected.to be_truthy }

  describe 'results' do
    before { command.call }

    subject { Message.last }

    it { is_expected.to be }
    it { expect(subject.author_name).to eq agent.name }
  end

  context 'when ticket has New status' do
    let!(:ticket) { create(:ticket, status: Ticket::NEW, agent: agent) }

    it do
      expect { subject }.to raise_error(FastTicket::Base::Error)
    end
  end

  context 'when ticket has Closed status' do
    let!(:ticket) { create(:ticket, status: Ticket::CLOSED, agent: agent) }

    it do
      expect { subject }.to raise_error(FastTicket::Base::Error)
    end
  end

  context 'when ticket belongs to other agent' do
    let!(:other_agent) { create(:agent) }
    let!(:ticket) { create(:ticket, status: Ticket::PROCESSING, agent: other_agent) }

    it do
      expect { subject }.to raise_error(FastTicket::Base::Error)
    end
  end
end
