require 'rails_helper'

describe FastTicket::Commands::Agents::ProcessTicket do
  let!(:agent) { create(:agent) }
  let!(:ticket) { create(:ticket, status: Ticket::NEW, agent: nil) }

  let(:command) do
    described_class.new(ticket_id: ticket.id, agent_id: agent.id)
  end

  subject { command.call }

  it { is_expected.to be_truthy }

  it do
    command.call
    expect(ticket.reload.status).to eq Ticket::PROCESSING
  end

  context 'when different status' do
    let!(:ticket) { create(:ticket, status: Ticket::PROCESSING, agent: nil) }

    it do
      expect { subject }.to raise_error(FastTicket::Base::Error)
    end
  end

  context 'when other agent in present' do
    let!(:other_agent) { create(:agent) }
    let!(:ticket) { create(:ticket, status: Ticket::NEW, agent: other_agent) }

    it do
      expect { subject }.to raise_error(FastTicket::Base::Error)
    end
  end
end
