require 'rails_helper'

describe FastTicket::Commands::Agents::CloseTicket do
  let!(:ticket) { create(:ticket, status: Ticket::NEW) }
  let(:agent_id) { ticket.agent.id }
  let(:command) do
    described_class.new(ticket_id: ticket.id, agent_id: agent_id)
  end

  subject { command.call }

  it { is_expected.to be_truthy }

  it do
    command.call
    expect(ticket.reload.status).to eq Ticket::CLOSED
  end

  context 'when agent invalid' do
    let(:agent_id) { create(:agent).id }

    it do
      expect { subject }.to raise_error(FastTicket::Base::Error)
    end
  end
end
