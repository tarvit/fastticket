require 'rails_helper'

describe FastTicket::Commands::Clients::SubmitTicket do
  let!(:client) { create(:client) }

  let(:attrs) do
    {
        title: 'title',
        description: 'title',
        category: 'Information'
    }
  end

  let(:params) do
    { client_id: client.id, ticket_attributes: attrs }
  end

  let(:command) do
    described_class.new(params)
  end

  subject { command.call }

  it { is_expected.to be_truthy }

  context 'results' do
    before { command.call }

    subject { Ticket.first }

    it { is_expected.to be }
    it { expect(subject.client).to eq client }
    it { expect(subject.status).to eq Ticket::NEW }
    it { expect(subject.attributes.symbolize_keys).to include(attrs) }
  end

  context 'when status passed' do
    before { command.call }

    let(:attrs) { super().merge(status: Ticket::PROCESSING) }

    subject { Ticket.first }

    it { expect(subject.status).to eq Ticket::NEW }
  end
end
