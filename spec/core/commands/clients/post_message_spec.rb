require 'rails_helper'

describe FastTicket::Commands::Clients::PostMessage do
  let!(:client) { create(:client) }
  let!(:ticket) { create(:ticket, status: Ticket::NEW, client: client) }
  let!(:conversation) { create(:conversation, ticket: ticket) }

  let(:command) do
    described_class.new(ticket_id: ticket.id, client_id: client.id, text: 'my text')
  end

  subject { command.call }

  it { is_expected.to be_truthy }

  describe 'results' do
    before { command.call }

    subject { Message.last }

    it { is_expected.to be }
    it { expect(subject.author_name).to eq client.name }
  end

  context 'when ticket has Processing status' do
    let!(:ticket) { create(:ticket, status: Ticket::PROCESSING, client: client) }

    it { is_expected.to be_truthy }
  end

  context 'when ticket has Closed status' do
    let!(:ticket) { create(:ticket, status: Ticket::CLOSED, client: client) }

    it do
      expect { subject }.to raise_error(FastTicket::Base::Error)
    end
  end

  context 'when ticket belongs to other client' do
    let!(:other_client) { create(:client) }
    let!(:ticket) { create(:ticket, status: Ticket::NEW, client: other_client) }

    it do
      expect { subject }.to raise_error(FastTicket::Base::Error)
    end
  end
end
