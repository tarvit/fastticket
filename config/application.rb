require_relative 'boot'

require 'rails/all'

Bundler.require(*Rails.groups)

module FastTicketApp
  class Application < Rails::Application

    config.react.addons = true

    config.autoload_paths << Rails.root.join('core')

    Rails.application.config.assets.precompile += %w( clients_application.js agents_application.js)

    config.react.server_renderer_pool_size  ||= 1
    config.react.server_renderer_timeout    ||= 20
    config.react.server_renderer = React::ServerRendering::SprocketsRenderer
    config.react.server_renderer_options = {
        files: %w(react-server.js clients_components.js agents_components.js),
        replay_console: true
    }
  end
end
