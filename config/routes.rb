Rails.application.routes.draw do
  root to: 'clients/dashboard#index'

  mount RailsAdmin::Engine => '/superadmin', as: 'rails_admin'

  %i(admins agents clients).each do |users|
    devise_for users, controllers: {
        sessions: 'custom_devise/sessions',
        registrations: 'custom_devise/registrations',
    }
  end

  scope :agents do
    get '/', to: 'agents/dashboard#index', as: :agent_dashboard
    get '/not_verified', to: 'agents/dashboard#not_verified', as: :agent_not_verified

    scope :reports do
      get '/', to: 'agents/reports#index'
    end
  end

  scope :api do
    get 'messages', to: 'api/messages#index'

    scope :clients do
      scope :tickets do
        get '/', to: 'api/clients/tickets#index'
        get '/:id', to: 'api/clients/tickets#show'
        post '/', to: 'api/clients/tickets#create'
      end

      scope :conversations do
        get '/:id', to: 'api/clients/conversations#show'
        post '/:id', to: 'api/clients/conversations#create'
      end
    end

    scope :agents do
      scope :tickets do
        get '/', to: 'api/agents/tickets#index'
        get '/:id', to: 'api/agents/tickets#show'
        patch '/:id/close', to: 'api/agents/tickets#close'
        patch '/:id/process', to: 'api/agents/tickets#process_ticket'
      end

      scope :conversations do
        get '/:id', to: 'api/agents/conversations#show'
        post '/:id', to: 'api/agents/conversations#create'
      end
    end
  end
end
