RailsAdmin.config do |config|

  config.authenticate_with do
    warden.authenticate! scope: :admin
  end

  config.actions do
    dashboard
    index
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app
  end
end

