class CreateTickets < ActiveRecord::Migration[5.0]
  def change
    create_table :tickets do |t|
      t.string :title
      t.string :category
      t.text :description

      t.integer :client_id
      t.integer :agent_id

      t.string :status

      t.timestamps
    end
  end
end
