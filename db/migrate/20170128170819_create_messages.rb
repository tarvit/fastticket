class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.text :text
      t.string :author_name
      t.integer :conversation_id, null: false

      t.timestamps
    end
  end
end
