class AddVerifiedToClient < ActiveRecord::Migration[5.0]
  def change
    add_column :agents, :verified, :boolean, default: false
  end
end
