class AddName < ActiveRecord::Migration[5.0]
  def change
    add_column :clients, :name, :string
    add_column :agents, :name, :string
  end
end
