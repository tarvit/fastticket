class RenameSubjectIdToClientId < ActiveRecord::Migration[5.0]
  def change
    remove_column :conversations, :subject_id, :string
    add_column :conversations, :ticket_id, :integer, null: false
  end
end
