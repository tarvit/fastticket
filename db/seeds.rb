if Admin.count.zero?
  Admin.create(email: 'superadmin@example.com', password: 'superadmin1990', password_confirmation: 'superadmin1990')
end

30.times do
  ticket = FactoryGirl.create(:ticket, status: Ticket::STATUSES.sample)
  conversation = FactoryGirl.create(:conversation, ticket: ticket)
  rand(15).times do
    FactoryGirl.create(:message, conversation: conversation)
  end
end
