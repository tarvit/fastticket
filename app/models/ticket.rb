class Ticket < ApplicationRecord
  NEW = 'new'.freeze
  PROCESSING = 'processing'.freeze
  CLOSED = 'closed'.freeze
  STATUSES = [NEW, PROCESSING, CLOSED].freeze

  belongs_to :client
  belongs_to :agent, required: false
  has_one :conversation
end
