class DeviseUser < ApplicationRecord
  self.abstract_class = true

  devise :database_authenticatable, :registerable,
         :rememberable, :trackable, :validatable

  validates :name, presence: true
end
