module FastTicket
  module Commands
    module Agents
      class ProcessTicket < FastTicket::Base::Command
        prepend Base::ExceptionInterceptor

        def initialize(ticket_id:, agent_id:)
          @ticket_id = ticket_id
          @agent_id = agent_id
        end

        def call
          record = Ticket.where(id: @ticket_id, agent_id: nil, status: Ticket::NEW).take
          record.update_attributes(status: Ticket::PROCESSING, agent_id: @agent_id)
        end
      end
    end
  end
end
