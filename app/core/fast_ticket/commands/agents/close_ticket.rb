module FastTicket
  module Commands
    module Agents
      class CloseTicket < FastTicket::Base::Command
        prepend Base::ExceptionInterceptor

        def initialize(ticket_id:, agent_id:)
          @ticket_id = ticket_id
          @agent_id = agent_id
        end

        def call
          record = Ticket.where(id: @ticket_id, agent_id: @agent_id).take
          record.update_attribute(:status, Ticket::CLOSED)
        end
      end
    end
  end
end
