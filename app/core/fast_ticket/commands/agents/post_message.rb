module FastTicket
  module Commands
    module Agents
      class PostMessage < FastTicket::Base::Command
        prepend Base::ExceptionInterceptor

        def initialize(agent_id:, ticket_id:, text:)
          @agent_id = agent_id
          @ticket_id = ticket_id
          @text = text
        end

        def call
          Message.create(author_name: author_name,
                         conversation_id: conversation_id, text: @text)
          true
        end

        private

        def conversation_id
          Ticket.where(id: @ticket_id, agent_id: @agent_id,
                       status: Ticket::PROCESSING).take.conversation.id
        end

        def author_name
          Agent.find(@agent_id).name
        end
      end
    end
  end
end
