module FastTicket
  module Commands
    module Clients
      class SubmitTicket < FastTicket::Base::Command
        prepend Base::ExceptionInterceptor

        def initialize(client_id:, ticket_attributes:)
          @client_id = client_id
          @ticket_attributes = ticket_attributes
        end

        def call
          ActiveRecord::Base.transaction do
            ticket = Ticket.where(client_id: @client_id).create(attributes)
            Conversation.create(ticket_id: ticket.id)
          end

          true
        end

        private

        def attributes
          @ticket_attributes.to_h.symbolize_keys
              .slice(:title, :description, :category)
              .merge(status: Ticket::NEW)
        end
      end
    end
  end
end
