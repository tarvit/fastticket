module FastTicket
  module Commands
    module Clients
      class PostMessage < FastTicket::Base::Command
        prepend Base::ExceptionInterceptor
        ALLOWED_STATUSES = [Ticket::NEW, Ticket::PROCESSING]

        def initialize(client_id:, ticket_id:, text:)
          @client_id = client_id
          @ticket_id = ticket_id
          @text = text
        end

        def call
          Message.create(author_name: author_name,
                         conversation_id: conversation_id, text: @text)
          true
        end

        private

        def conversation_id
          Ticket.where(id: @ticket_id, client_id: @client_id,
                       status: ALLOWED_STATUSES).take.conversation.id
        end

        def author_name
          Client.find(@client_id).name
        end
      end
    end
  end
end
