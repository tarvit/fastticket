module FastTicket
  module Queries
    module Agents
      class GetTickets < FastTicket::Base::Query
        prepend Base::ExceptionInterceptor

        def initialize(agent_id:, filters:)
          @agent_id = agent_id
          @filters = filters
        end

        def call
          Ticket.where(query_options).order(id: :desc).map do |t|
            FastTicket::Queries::Results::Ticket.new(t).as_hash
          end
        end

        private

        def query_options
          TicketFilters::QueryBuilder.new(
              agent_id: @agent_id, params: @filters
          ).query_options
        end
      end
    end
  end
end
