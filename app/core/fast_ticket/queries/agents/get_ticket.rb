module FastTicket
  module Queries
    module Agents
      class GetTicket < FastTicket::Base::Query
        prepend Base::ExceptionInterceptor

        def initialize(ticket_id:)
          @ticket_id = ticket_id
        end

        def call
          record = Ticket.where(id: @ticket_id).take
          FastTicket::Queries::Results::Ticket.new(record).as_hash
        end
      end
    end
  end
end
