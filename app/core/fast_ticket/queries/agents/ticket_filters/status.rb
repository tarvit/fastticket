module FastTicket
  module Queries
    module Agents
      module TicketFilters
        class Status < BaseFilter
          def query_options
            { status: @value }
          end
        end
      end
    end
  end
end
