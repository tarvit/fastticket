module FastTicket
  module Queries
    module Agents
      module TicketFilters
        class Agent < BaseFilter

          def query_options
            { agent_id: options[@value] }
          end

          private

          def options
            {
                'mine' => @agent_id,
                'not_assigned' => nil,
            }
          end
        end
      end
    end
  end
end
