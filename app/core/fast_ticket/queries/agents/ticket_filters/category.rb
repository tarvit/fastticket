module FastTicket
  module Queries
    module Agents
      module TicketFilters
        class Category < BaseFilter

          def query_options
            { category: @value }
          end
        end
      end
    end
  end
end
