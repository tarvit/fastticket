module FastTicket
  module Queries
    module Agents
      module TicketFilters
        class BaseFilter
          def initialize(value:, agent_id:)
            @value = value
            @agent_id = agent_id
          end

          def query_options
            fail NotImplementedError
          end
        end
      end
    end
  end
end
