module FastTicket
  module Queries
    module Agents
      module TicketFilters
        class QueryBuilder
          ANY = 'any'.freeze

          FILTERS = {
              period: TicketFilters::Period,
              status: TicketFilters::Status,
              agent: TicketFilters::Agent,
              category: TicketFilters::Category
          }.freeze

          attr_reader :params, :agent_id

          def initialize(agent_id:, params:)
            @params = params
            @agent_id = agent_id
          end

          def query_options
            FILTERS.each_with_object({}) do |(filter_name, filter_class), res|
              value = params[filter_name]
              if value && value != ANY
                opts = filter_class.new(value: value, agent_id: agent_id).query_options
                res.merge!(opts)
              end
            end
          end
        end
      end
    end
  end
end
