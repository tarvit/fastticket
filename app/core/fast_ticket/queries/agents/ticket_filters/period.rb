module FastTicket
  module Queries
    module Agents
      module TicketFilters
        class Period < BaseFilter

          def query_options
            { updated_at: options[@value] }
          end

          private

          def options
            {
                'today' => DateTime.now.beginning_of_day..DateTime.now.end_of_day,
                'last_week' => DateTime.now.beginning_of_week..DateTime.now.end_of_day,
                'last_month' => DateTime.now.beginning_of_month..DateTime.now.end_of_day,
            }
          end
        end
      end
    end
  end
end
