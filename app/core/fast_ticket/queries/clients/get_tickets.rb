module FastTicket
  module Queries
    module Clients
      class GetTickets < FastTicket::Base::Query
        prepend Base::ExceptionInterceptor

        def initialize(client_id:)
          @client_id = client_id
        end

        def call
          Ticket.where(client_id: @client_id).order(id: :desc).map do |t|
            FastTicket::Queries::Results::Ticket.new(t).as_hash
          end
        end
      end
    end
  end
end
