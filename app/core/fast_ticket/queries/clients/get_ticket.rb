module FastTicket
  module Queries
    module Clients
      class GetTicket < FastTicket::Base::Query
        prepend Base::ExceptionInterceptor

        def initialize(client_id:, ticket_id:)
          @client_id = client_id
          @ticket_id = ticket_id
        end

        def call
          record = Ticket.where(client_id: @client_id, id: @ticket_id).take
          if record
            FastTicket::Queries::Results::Ticket.new(record).as_hash
          else
            nil
          end
        end
      end
    end
  end
end
