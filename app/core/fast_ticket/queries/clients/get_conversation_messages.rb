module FastTicket
  module Queries
    module Clients
      class GetConversationMessages < FastTicket::Base::Query
        prepend Base::ExceptionInterceptor

        def initialize(client_id:, ticket_id:)
          @client_id = client_id
          @ticket_id = ticket_id
        end

        def call
          record = Ticket.where(client_id: @client_id, id: @ticket_id).take.conversation
          Results::Conversation.new(record).as_hash
        end
      end
    end
  end
end
