module FastTicket
  module Queries
    module Results
      class TicketClient < FastTicket::Base::QueryResult
        def as_hash
          { name: object.name }
        end
      end
    end
  end
end
