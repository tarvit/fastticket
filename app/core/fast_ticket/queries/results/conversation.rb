module FastTicket
  module Queries
    module Results
      class Conversation < FastTicket::Base::QueryResult
        def as_hash
          {
            messages: object.messages.order(id: :desc).map { |m| Results::Message.new(m).as_hash }
          }
        end
      end
    end
  end
end
