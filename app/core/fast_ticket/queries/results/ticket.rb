module FastTicket
  module Queries
    module Results
      class Ticket < FastTicket::Base::QueryResult
        def as_hash
          {
              id: object.id,
              title: object.title,
              category: object.category,
              description: object.description,
              created_at: object.created_at.strftime('%B %d'),
              support_agent: TicketAgent.new(object.agent).as_hash,
              requester: TicketClient.new(object.client).as_hash,
              status: Status.new(object.status).as_hash
          }
        end
      end
    end
  end
end
