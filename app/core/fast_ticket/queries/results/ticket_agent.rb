module FastTicket
  module Queries
    module Results
      class TicketAgent < FastTicket::Base::QueryResult
        def as_hash
          { name: name, id: object&.id }
        end

        private

        def name
          object.present? ? object.name : 'Not assigned'
        end
      end
    end
  end
end
