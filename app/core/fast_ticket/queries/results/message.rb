module FastTicket
  module Queries
    module Results
      class Message < FastTicket::Base::QueryResult
        def as_hash
          {
              id: object.id,
              text: object.text,
              author_name: object.author_name,
              conversation_id: object.conversation_id,
              created_at: object.created_at.strftime('%B %d'),
              timestamp: object.created_at.to_i
          }
        end
      end
    end
  end
end
