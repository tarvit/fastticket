module FastTicket
  module Queries
    module Results
      class Status < FastTicket::Base::QueryResult
        def as_hash
          {
              value: object, label: object.to_s.humanize
          }
        end
      end
    end
  end
end
