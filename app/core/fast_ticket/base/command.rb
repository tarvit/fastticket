module FastTicket
  module Base
    class Command
      def call
        fail NotImplementedError
      end
    end
  end
end
