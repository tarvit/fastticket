module FastTicket
  module Base
    class Query
      def call
        fail NotImplementedError
      end
    end
  end
end
