module FastTicket
  module Base
    module ExceptionInterceptor
      def call
        super
      rescue StandardError => ex
        Rails.logger.error("Exception: #{ex.class}\nMessage: #{ex.message}\nBacktrace: #{ex.backtrace.join("\n")}")

        raise Base::Error
      end
    end
  end
end
