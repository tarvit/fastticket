module FastTicket
  module Base
    class QueryResult
      attr_reader :object

      def initialize(object)
        @object = object
      end

      def as_hash
        fail NotImplementedError
      end
    end
  end
end
