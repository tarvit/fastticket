module Agents
  class ReportsController < ApplicationController
    check_agent_access!

    def index
      @filters = (params[:filters] || {})
      query = FastTicket::Queries::Agents::GetTickets.new(agent_id: current_agent.id,
                                                          filters: @filters)
      @report = query.call
      render pdf: 'index'
    end
  end
end
