module Agents
  class DashboardController < ApplicationController
    layout 'main'

    check_agent_access!

    skip_before_action :authenticate_agent!,
                       :check_agent_verified!, only: :not_verified

    def index; end
    def not_verified; end
  end
end

