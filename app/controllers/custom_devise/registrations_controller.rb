module CustomDevise
  class RegistrationsController < Devise::RegistrationsController
    layout 'main'

    def sign_up_params
      super.merge('name' => params[resource_name][:name] )
    end
  end
end

