module Api
  module Agents
    class TicketsController < Api::BaseController
      check_api_agent_access!

      def index
        filters = (params[:filters] || {})
        query = FastTicket::Queries::Agents::GetTickets.new(agent_id: current_agent.id,
                                                            filters: filters)
        render json: { tickets: query.call }
      end

      def show
        query = FastTicket::Queries::Agents::GetTicket.new(ticket_id: params[:id])
        render json: { ticket: query.call }
      end

      def process_ticket
        command = FastTicket::Commands::Agents::ProcessTicket.new(ticket_id: params[:id],
                                                                  agent_id: current_agent.id)
        render json: { result: command.call }
      end

      def close
        command = FastTicket::Commands::Agents::CloseTicket.new(ticket_id: params[:id],
                                                                agent_id: current_agent.id)
        render json: { result: command.call }
      end
    end
  end
end
