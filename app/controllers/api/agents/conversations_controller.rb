module Api
  module Agents
    class ConversationsController < BaseController
      check_api_agent_access!

      def show
        query = FastTicket::Queries::Agents::GetConversationMessages.new(ticket_id: params[:id])
        render json: { conversation: query.call }
      end

      def create
        command = FastTicket::Commands::Agents::PostMessage.new(agent_id: current_agent.id,
                                                                ticket_id: params[:id],
                                                                text: params[:message])
        render json: { result: command.call }, status: :created
      end
    end
  end
end
