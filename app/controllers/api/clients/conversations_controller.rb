module Api
  module Clients
    class ConversationsController < BaseController
      check_api_client_access!

      def show
        query = FastTicket::Queries::Clients::GetConversationMessages.new(client_id: current_client.id,
                                                                          ticket_id: params[:id])
        render json: { conversation: query.call }
      end

      def create
        command = FastTicket::Commands::Clients::PostMessage.new(client_id: current_client.id,
                                                                 ticket_id: params[:id],
                                                                 text: params[:message])
        render json: { result: command.call }, status: :created
      end
    end
  end
end
