module Api
  module Clients
    class TicketsController < Api::BaseController
      check_api_client_access!

      def index
        query = FastTicket::Queries::Clients::GetTickets.new(client_id: current_client.id)
        render json: { tickets: query.call }
      end

      def create
        command = FastTicket::Commands::Clients::SubmitTicket.new(
          client_id: current_client.id,
          ticket_attributes: params.require(:ticket).permit(:title, :category, :description)
        )
        render json: { result: command.call }
      end

      def show
        query = FastTicket::Queries::Clients::GetTicket.new(client_id: current_client.id, ticket_id: params[:id])
        render json: { ticket: query.call }
      end
    end
  end
end
