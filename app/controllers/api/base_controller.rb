module Api
  class BaseController < ApplicationController
    respond_to :json

    rescue_from Exception do |e|
      handle_error(e)
    end

    class << self
      def check_api_agent_access!
        before_action do
          if current_agent.nil? || !current_agent.verified?
            render json: { message: 'You must log in as a agent first' }, status: 401
          end
        end
      end

      def check_api_client_access!
        before_action do
          if current_client.nil?
            render json: { message: 'You must log in as a client first' }, status: 401
          end
        end
      end
    end

    protected

    def handle_error(e)
      if request.path =~ /^\/api/
        error_info = { error: 'InternalServerError' }

        if Rails.env.development? || Rails.env.test?
          error_info.merge!(
              exception: {
                  class: e.class.name,
                  message: e.message,
                  trace: e.backtrace[0, 10]
              }
          )
        end
        render json: error_info.to_json, status: 500
      else
        raise e
      end
    end
  end
end
