class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  class << self
    def check_agent_access!
      before_action :authenticate_agent!
      before_action :check_agent_verified!
    end
  end

  def check_agent_verified!
    unless current_agent.verified?
      redirect_to agent_not_verified_path
    end
  end
end
