module Clients
  class DashboardController < ApplicationController
    layout 'main'

    before_action :authenticate_client!

    def index;  end
  end
end