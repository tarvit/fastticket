var AgentRouter = new StateViewRouter();

AgentRouter.setDefaultView('tickets_list');
AgentRouter.addRoute('', 'tickets_list');
AgentRouter.addRoute('tickets/:ticketId', 'tickets_item');
