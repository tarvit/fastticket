var ClientRouter = new StateViewRouter();

ClientRouter.setDefaultView('tickets_list');
ClientRouter.addRoute('', 'tickets_list');
ClientRouter.addRoute('tickets/create', 'tickets_create');
ClientRouter.addRoute('tickets/created', 'tickets_created');
ClientRouter.addRoute('tickets/:ticketId', 'tickets_item');
