var StateViewRouter = function(){
    this.routes = [];
    this.defaultView = null;

    this.addRoute = function(route, view) {
        this.routes.push({route: route, view: view})
    };

    this.setDefaultView = function(view){
        this.defaultView = view;
    };

    this.currentState = function(){
        return this.resolve(window.location.hash.replace('#', ''))
    };

    this.resolve = function(route){
        var currentParts = route.split('/');

        for(var i = 0; i < this.routes.length; i++) {
            var parts = this.routes[i].route.split('/');
            if(parts.length == currentParts.length) {
                var params = { view: this.routes[i].view };
                var valid = true;

                for(var j =0; j < parts.length; j++) {
                    var cp = currentParts[j];
                    var p = parts[j];
                    if(p[0] == ':') {
                        params[p.replace(':', '')] = cp;
                    } else {
                        if(p != cp) {
                            valid = false;
                            break
                        }
                    }
                }
                if(valid) { return params }
            }
        }

        return { view: this.defaultView }
    };

    this.navigate = function(viewState){
        var path = this.buildPath(viewState);
        window.location.hash = path;
    };

    this.buildPath = function(viewState) {
        var route = this.routeForView(viewState.view);

        var path = route.route;
        var parts = path.split('/');
        var params = parts.filter(function(e){ return e[0] == ':' }).map(function(e){return e.slice(1) });

        for(var i = 0; i < params.length; i++ ){
            path = path.replace(':' + params[i], viewState[params[i]])
        }

        return path;
    };

    this.routeForView = function(view){
        for(var i = 0; i < this.routes.length; i++) {
            if(this.routes[i].view == view) {
                return this.routes[i]
            }
        }
    };
};
