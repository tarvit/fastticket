var AgentTicketsStore = function(){
    var self = this;
    this.tickets = null;
    this.detailedTickets = {};

    this.loadFiltered = function(){
        $.getJSON('/api/agents/tickets.json',
            {
                filters: Store.AgentTicketFilters.getCurrentFilters()
            },
            function(response) {
                self.tickets = response.tickets;
                self.trigger('tickets.filtered.loaded')
            }
        );
    };

    this.getFiltered = function(){
        return this.tickets;
    };

    this.closeTicket = function(ticketId) {
        var self = this;
        $.ajax({
            url: '/api/agents/tickets/' + ticketId + '/close.json',
            type: 'PATCH'
        }).success(
            function() {
                self.loadTicket(ticketId)
            }
        );
    };

    this.processTicket = function(ticketId) {
        var self = this;
        $.ajax({
            url: '/api/agents/tickets/' + ticketId + '/process.json',
            type: 'PATCH'
        }).success(
            function() {
                self.loadTicket(ticketId)
            }
        );
    };

    this.loadTicket = function(ticketId) {
        $.getJSON('/api/agents/tickets/'+ticketId+'.json', function(response) {
            self.detailedTickets[ticketId] = response.ticket;
            self.trigger('ticket.'+ticketId+'.loaded');
            self.refreshTicketsItem(response.ticket);
        });
    };

    this.getTicket = function(ticketId){
        return this.detailedTickets[ticketId];
    };

    this.refreshTicketsItem = function(ticket){
        if (!this.tickets) return;
            for(var i=0; i < this.tickets.length; i++){
            if(this.tickets[i].id == ticket.id){
                this.tickets[i] = ticket;
                self.trigger('tickets.filtered.loaded');
            }
        }
    };

    this.downloadFilteredPDF = function(){
        var params = { filters: Store.AgentTicketFilters.getCurrentFilters() };
        var url = '/agents/reports?' + $.param(params);
        window.location = url;
    }
};

MicroEvent.mixin(AgentTicketsStore);
