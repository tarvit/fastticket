var AgentConversationsStore = function() {
    var self = this;
    this.conversations = {};

    this.loadConversation = function(ticketId) {
        $.getJSON('/api/agents/conversations/'+ticketId+'.json', function(response) {
            self.conversations[ticketId] = response.conversation;
            self.trigger('conversation.'+ticketId+'.loaded')
        });
    };

    this.getConversation = function(ticketId){
        return this.conversations[ticketId];
    };

    this.postMessage = function(ticketId, message, callback) {
        var self = this;
        $.post('/api/agents/conversations/'+ticketId+'.json',
            {
                message: message
            },
            function() {
                callback();
                self.loadConversation(ticketId)
            }
        );
    }
};

MicroEvent.mixin(AgentConversationsStore);
