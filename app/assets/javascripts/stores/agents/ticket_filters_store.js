var AgentTicketFiltersStore = function(){
    var self = this;

    this.currentFilters = {};
    this.allOptions = null;

    this.loadAllOptions = function(){
        self.allOptions = StaticFilters;
        self.trigger('filters.loaded')
    };

    this.getAllOptions = function(){
        return this.allOptions;
    };

    this.updateCurrentFilters = function(options){
        this.currentFilters = options;
    };

    this.getCurrentFilters = function(){
        return this.currentFilters;
    };
};

MicroEvent.mixin(AgentTicketFiltersStore);
