var categoryOptions = [{ label: "Any", value: "any" }];

for(var i = 0; i < StaticCategories.length; i++) {
    var value = StaticCategories[i].name;
    categoryOptions.push({label: value, value: value})
}

var StaticFilters = [
    {
        label: "Status",
        name: "status",
        options: [
            { label: "Any", value: "any"},
            { label: "new", value: "new"},
            { label: "processing", value: "processing"},
            { label: "closed", value: "closed" }
        ]
    },
    {
        label: "Agent",
        name: "agent",
        options: [
            { label: "Any", value: "any" },
            { label: "Mine", value: "mine" },
            { label: "Not Assigned", value: "not_assigned" }
        ]
    },
    {
        label: "Period",
        name: "period",
        options: [
            { label: "Any", value: "any" },
            { label: "Today", value: "today" },
            { label: "Last Week", value: "last_week" },
            { label: "Last Month", value: "last_month" }
        ]
    },
    {
        label: "Category",
        name: "category",
        options: categoryOptions
    }
];
