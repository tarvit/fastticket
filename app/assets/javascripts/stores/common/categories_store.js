var CategoriesStore = function() {
    var self = this;
    this.categories = null;

    this.loadAll = function() {
        this.categories = StaticCategories;
        this.trigger('categories.loaded');
    };

    this.getAll = function(){
        return this.categories;
    };
};

MicroEvent.mixin(CategoriesStore);
