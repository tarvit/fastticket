var ClientTicketsStore = function(){
    var self = this;
    this.tickets = null;
    this.detailedTickets = {};

    this.loadAll = function(){
        $.getJSON('/api/clients/tickets.json', function(response) {
            self.tickets = response.tickets;
            self.trigger('tickets.loaded')
        });
    };

    this.getAll = function(){
        return this.tickets;
    };

    this.createTicket = function(ticket) {
        $.post('/api/clients/tickets.json',
            { ticket: ticket },
            function(response) {
        });
    };

    this.loadTicket = function(ticketId) {
        $.getJSON('/api/clients/tickets/'+ticketId+'.json', function(response) {
            self.detailedTickets[ticketId] = response.ticket;
            self.trigger('ticket.'+ticketId+'.loaded')
        });
    };

    this.getTicket = function(ticketId){
        return this.detailedTickets[ticketId];
    };
};

MicroEvent.mixin(ClientTicketsStore);
