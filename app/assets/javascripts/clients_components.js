//= require microevent
//= require_tree ./stores/common
//= require_tree ./stores/clients
//= require ./routes/state_router
//= require ./routes/config/clients
//= require_self
//= require_tree ./components/widgets/clients
//= require_tree ./components/widgets/common

var Store = {};
Store.ClientTickets = new ClientTicketsStore();
Store.ClientConversations = new ClientConversationsStore();
Store.Categories = new CategoriesStore();

