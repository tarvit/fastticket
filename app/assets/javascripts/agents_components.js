//= require microevent
//= require_tree ./stores/common
//= require_tree ./stores/agents
//= require ./routes/state_router
//= require ./routes/config/agents
//= require_self
//= require_tree ./components/widgets/agents
//= require_tree ./components/widgets/common

var Store = {};
Store.AgentTickets = new AgentTicketsStore();
Store.AgentConversations = new AgentConversationsStore();
Store.AgentTicketFilters = new AgentTicketFiltersStore();
Store.Categories = new CategoriesStore();

