var AgentsMain = React.createClass({
    getInitialState() {
        var self = this;
        window.onhashchange = function(){
            self.navigate(AgentRouter.currentState());
        };
        return AgentRouter.currentState();
    },

    navigate(viewState){
        this.setState(viewState);
        AgentRouter.navigate(viewState);
    },

    viewTicket(ticketId) {
        this.navigate({ view: 'tickets_item', ticketId: ticketId });
    },

    viewTickets() {
        this.navigate({ view: 'tickets_list' });
    },

    render() {
        return (
            <div className="clients-main-widget">
                {
                    this.state.view == 'tickets_list' &&
                    <AgentTicketsList viewTicket={this.viewTicket} />
                }
                {
                    this.state.view == 'tickets_item' &&
                    <AgentTicketItem ticketId={this.state.ticketId}
                                      viewTickets={this.viewTickets}
                                      createTicket={this.createTicket} />
                }
            </div>
        )
    }
});
