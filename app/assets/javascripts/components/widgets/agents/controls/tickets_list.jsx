var AgentTicketsList = React.createClass({
    getInitialState() {
        return { tickets: Store.AgentTickets.getFiltered(), selection: {} }
    },

    componentDidMount() {
        Store.AgentTickets.bind('tickets.filtered.loaded', this.updateTickets);
        Store.AgentTickets.loadFiltered();
    },

    componentWillUnmount: function() {
        Store.AgentTickets.unbind('tickets.filtered.loaded', this.updateTickets);
    },

    updateTickets() {
        this.setState({ tickets: Store.AgentTickets.getFiltered() });
    },

    reloadTickets(options) {
        Store.AgentTicketFilters.updateCurrentFilters(options);
        Store.AgentTickets.loadFiltered();
    },

    viewTicketById(id){
        let self = this;
        return function(){ self.props.viewTicket(id) }
    },

    downloadPdf(){
        Store.AgentTickets.downloadFilteredPDF()
    },

    render() {
        return (
            <div>
                <NavLinks links={ [
                    {onClick: this.downloadPdf, text: 'Download Report in PDF'}
                ]}/>

                <hr/>
                <div className="panel panel-default">
                    <div className="panel-heading">Tickets</div>
                    {
                        !this.state.tickets && <div className="panel-body">
                            <Loading/>
                        </div>
                    }
                    {
                        this.state.tickets &&
                        <div>
                            <AgentTicketFilters updateTickets={this.reloadTickets} selection={this.state.selection} />
                            <div className="ft-table-container">
                            <table className="table ft-tickets-list">
                            <thead>
                                <tr>
                                    <th>Ticket ID</th>
                                    <th>Title</th>
                                    <th className="ft-xs-hide">Category</th>
                                    <th className="ft-xs-hide">Requester</th>
                                    <th className="ft-xs-hide">Agent</th>
                                    <th className="ft-xs-hide">Created At</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.tickets.map((ticket) =>
                                    <tr key={ticket.id}>
                                        <th scope="row">{ticket.id}</th>
                                        <td>{ticket.title}</td>
                                        <td className="ft-xs-hide">{ticket.category}</td>
                                        <td className="ft-xs-hide">{ticket.requester.name}</td>
                                        <td className="ft-xs-hide">{ticket.support_agent.name}</td>
                                        <td className="ft-xs-hide">{ticket.created_at}</td>
                                        <td className={ticket.status.value + " status ft-xs-hide"}>
                                            {ticket.status.label}
                                        </td>
                                        <td>
                                            <AgentTicketActions ticket={ticket} viewTicket={this.viewTicketById(ticket.id)}/>
                                        </td>
                                    </tr>)
                                }
                            </tbody>
                            </table>
                        </div>
                        </div>
                    }
                </div>
            </div>
        )
    }
});
