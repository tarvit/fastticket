var AgentTicketItem = React.createClass({
    getInitialState() {
        return {ticket: Store.AgentTickets.getTicket(this.props.ticketId), ticketId: this.props.ticketId}
    },

    componentDidMount() {
        Store.AgentTickets.bind('ticket.'+this.props.ticketId+'.loaded', this.updateTicket);
        Store.AgentTickets.loadTicket(this.props.ticketId);
    },

    componentWillUnmount: function() {
        Store.AgentTickets.unbind('ticket.'+this.props.ticketId+'.loaded', this.updateTicket);
    },

    updateTicket(){
        this.setState({ ticket: Store.AgentTickets.getTicket(this.props.ticketId) });
    },

    processingStatus(){
        return (this.state.ticket && this.state.ticket.status.value == 'processing')
    },

    ownedByAgent(){
        return (this.state.ticket && CurrentAgent.id == this.state.ticket.support_agent.id)
    },
    render() {
        return (
            <div>
                <div className="col-md-12">
                    <NavLinks links={ [
                        {onClick: this.props.viewTickets, text: 'Back To Tickets'}
                    ]}/>

                    <h1 id="ft-ticket-title">
                        Ticket#
                        <span>{this.state.ticketId}</span>
                    </h1>
                    <hr/>
                </div>
                <div className="col-md-5">
                    {!this.state.ticket && <Loading/> }
                    {this.state.ticket &&
                        <TicketDetails ticket={this.state.ticket}/>
                    }
                    {this.state.ticket &&
                        <AgentTicketActions ticket={this.state.ticket}/>
                    }
                </div>

                <div className="col-md-5">
                    <Conversation ticketId={this.state.ticketId}
                                  postingEnabled={this.ownedByAgent() && this.processingStatus()}
                                  conversationsStore={Store.AgentConversations}/>
                </div>
            </div>
        )}
});
