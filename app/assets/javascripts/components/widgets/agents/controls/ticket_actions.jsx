var AgentTicketActions = React.createClass({
    ownedByAgent(){
        return (CurrentAgent.id == this.props.ticket.support_agent.id)
    },
    canProcess(){
        return (this.props.ticket.support_agent.id == null &&
            this.props.ticket.status.value != 'processing')
    },
    canClose(){
        return (this.ownedByAgent() && (this.props.ticket.status.value != 'closed'))
    },
    closeTicket(){
        Store.AgentTickets.closeTicket(this.props.ticket.id);
    },
    processTicket(){
        Store.AgentTickets.processTicket(this.props.ticket.id);
    },
    render() {
        return (
            <div>
                <div className="btn-group btn-group-xs" role="group">
                    {this.props.viewTicket &&
                        <button className="ft-view-btn btn btn-success" onClick={this.props.viewTicket}>
                            View
                        </button>
                    }
                    {this.canProcess() &&
                        <button className="ft-process-btn btn btn-danger" onClick={this.processTicket}>
                            Process
                        </button>
                    }
                    {this.canClose() &&
                        <button className="ft-close-btn btn btn-default" onClick={this.closeTicket}>
                            Close
                        </button>
                    }
                </div>
            </div>
        )
    }
});
