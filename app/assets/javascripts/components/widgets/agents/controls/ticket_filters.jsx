var AgentTicketFilters = React.createClass({
    getInitialState() {
        return {
            filters: Store.AgentTicketFilters.getAllOptions()
        }
    },
    componentDidMount() {
        Store.AgentTicketFilters.bind('filters.loaded', this.updateOptions);
        Store.AgentTicketFilters.loadAllOptions();
    },

    componentWillUnmount: function() {
        Store.AgentTicketFilters.unbind('filters.loaded', this.updateOptions);
    },

    updateOptions: function(){
        this.setState({ filters: Store.AgentTicketFilters.getAllOptions() });
    },

    handleSubmit: function(event){
      event.preventDefault();
    },

    handleInputChange(event) {
        this.props.selection[event.target.id] = event.target.value;
        this.props.updateTickets(this.props.selection)
    },

    render() {
        return (
            <div className="col-md-12 ft-ticket-filters">
                {
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        {   this.state.filters &&
                            this.state.filters.map((filter) =>
                            <div className="col-md-3" key={filter.name}>
                                <label htmlFor={filter.name}>{filter.label}</label>
                                <select className="form-control" id={filter.name}
                                        value={this.state.category} onChange={this.handleInputChange}>
                                    {
                                        filter.options.map((option) =>
                                            <option key={option.label} value={option.value}>{option.label}</option>
                                        )
                                    }
                                </select>
                            </div>
                            )
                        }
                    </div>
                </form>
                }
            </div>
        )}
});
