var NavLinks = React.createClass({
    render() {
        return(
            <div className="btn-toolbar">
                {
                    this.props.links.map((link) =>
                        <div className="btn-group" role="group" key={link.text}>
                            <button className="btn btn-success" onClick={link.onClick}>
                                {link.text}
                            </button>
                        </div>
                    )
                }
            </div>
        )
    }
});
