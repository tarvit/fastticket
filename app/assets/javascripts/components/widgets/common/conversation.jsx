var Conversation = React.createClass({
    getInitialState() {
        return {
            conversation: this.props.conversationsStore.getConversation(this.props.ticketId),
            ticketId: this.props.ticketId
        }
    },

    componentDidMount() {
        this.props.conversationsStore.loadConversation(this.props.ticketId);
        this.props.conversationsStore
            .bind('conversation.'+this.props.ticketId+'.loaded', this.updateConversation);
    },

    componentWillUnmount: function() {
        this.props.conversationsStore
            .unbind('conversation.'+this.props.ticketId+'.loaded', this.updateConversation);
    },

    updateConversation(){
        this.setState({ conversation: this.props.conversationsStore.getConversation(this.props.ticketId) });
    },

    render() {
        return(
            <div>
                { !this.state.conversation && <Loading/> }
                { this.state.conversation &&
                    <div>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">Conversation</h3>
                            </div>

                            <div className="panel-body">

                                { this.props.postingEnabled &&
                                    <PostMessage ticketId={this.props.ticketId}
                                                 conversationsStore={this.props.conversationsStore}/>
                                }
                                {
                                    this.state.conversation.messages.map((message) =>
                                        <ConversationMessage key={message.id} message={message}/>
                                    )
                                }
                            </div>
                        </div>
                    </div>
                }
            </div>
        )
    }
});
