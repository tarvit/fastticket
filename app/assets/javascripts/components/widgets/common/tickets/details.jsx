var TicketDetails = React.createClass({
    render() {
        return (
            <div className="panel panel-default ticket-details">
                <div className="panel-heading">
                    <h3 className="panel-title">
                        <p>Title:</p>
                        <span className="title">{this.props.ticket.title}</span>
                    </h3>
                </div>
                <div className="panel-body">
                    <p>Description:</p>
                    <span className="description">{this.props.ticket.description}</span>
                </div>
                <div className="panel-footer">
                    <p>Status:</p>
                    <span className={this.props.ticket.status.value + " status"} >
                        {this.props.ticket.status.label}
                    </span>
                </div>
            </div>
        )}
});