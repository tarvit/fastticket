var ConversationMessage = React.createClass({
    getInitialState() {
        return {message: this.props.message}
    },

    render() {
        return(
            <div className="alert alert-info ft-message">
                <p className="text">
                    {this.props.message.text}
                </p>

                <p className="about">
                    <span>{this.props.message.author_name}, {this.props.message.created_at}</span>
                </p>
            </div>
        )
    }
});
