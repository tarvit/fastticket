var PostMessage = React.createClass({
    getInitialState() {
        return { inProgress: false}
    },

    handleInputChange(event) {
        this.setState({ message: event.target.value});
    },

    submitEnabled(){
        return this.state.message && !this.state.inProgress
    },

    handleSubmit(event){
        event.stopPropagation();
        this.props.conversationsStore.postMessage(
            this.props.ticketId,
            this.state.message,
            this.inProgressFinished
        )
    },

    inProgressFinished(){
        this.setState({ inProgress: false, message: ''});
        $('#post_input').val(null)
    },

    render() {
        return(
            <div className="ft-post-message">
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <input type="title" className="form-control" id="post_input"
                               onChange={this.handleInputChange}
                        />
                    </div>
                    <button type="submit" className="btn btn-success"
                            title={this.submitEnabled() ? "" : "Type a message first"}
                            disabled={!this.submitEnabled()}>Post</button>
                </form>
                <hr/>
            </div>
        )
    }
});
