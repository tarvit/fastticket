var ClientsMain = React.createClass({
    getInitialState() {
        var self = this;
        window.onhashchange = function(){
            self.navigate(ClientRouter.currentState());
        };
        return ClientRouter.currentState();
    },

    navigate(viewState){
        this.setState(viewState);
        ClientRouter.navigate(viewState);
    },

    viewTicket(ticketId) {
        this.navigate({ view: 'tickets_item', ticketId: ticketId });
    },

    viewTickets() {
        this.navigate({ view: 'tickets_list' });
    },

    createTicket() {
        this.navigate({ view: 'tickets_create' });
    },

    createdTicket() {
        this.navigate({ view: 'tickets_created' });
    },

    render() {
        return (
            <div className="clients-main-widget">
                {
                    this.state.view == 'tickets_list' &&
                    <ClientTicketsList viewTicket={this.viewTicket}
                                       createTicket={this.createTicket} />
                }
                {
                    this.state.view == 'tickets_item' &&
                    <ClientTicketItem ticketId={this.state.ticketId}
                                      viewTickets={this.viewTickets}
                                      createTicket={this.createTicket} />
                }
                {
                    this.state.view == 'tickets_create' &&
                    <ClientTicketItemCreate viewTickets={this.viewTickets}
                                            createdTicket={this.createdTicket}/>
                }
                {
                    this.state.view == 'tickets_created' &&
                    <ClientTicketItemCreated viewTickets={this.viewTickets}
                                             createTicket={this.createTicket} />
                }
            </div>
        )
    }
});
