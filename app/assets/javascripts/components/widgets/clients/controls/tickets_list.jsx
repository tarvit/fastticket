var ClientTicketsList = React.createClass({
    getInitialState() {
        return { tickets: Store.ClientTickets.getAll() }
    },

    componentDidMount() {
        Store.ClientTickets.loadAll();
        Store.ClientTickets.bind('tickets.loaded', this.updateTickets);
    },

    componentWillUnmount: function() {
        Store.ClientTickets.unbind('tickets.loaded', this.updateTickets);
    },

    updateTickets() {
        this.setState({ tickets: Store.ClientTickets.getAll() });
    },

    viewTicketById(id){
        let self = this;
        return function(){ self.props.viewTicket(id) }
    },

    render() {
        return (
            <div>
                <NavLinks links={ [{onClick: this.props.createTicket, text: 'Submit a Ticket'}] }/>
                <hr/>
                <div className="panel panel-default">
                    <div className="panel-heading">My Tickets</div>
                    {
                        !this.state.tickets && <div className="panel-body">
                            <Loading/>
                        </div>
                    }
                    {
                        this.state.tickets && <table className="table ft-tickets-list">
                        <thead>
                            <tr>
                                <th>Ticket ID</th>
                                <th>Title</th>
                                <th className="ft-xs-hide">Category</th>
                                <th className="ft-xs-hide">Support Agent</th>
                                <th className="ft-xs-hide">Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.tickets.map((ticket) =>
                                <tr key={ticket.id}>
                                    <th scope="row">{ticket.id}</th>
                                    <td>{ticket.title}</td>
                                    <td className="ft-xs-hide">{ticket.category}</td>
                                    <td className="ft-xs-hide">{ticket.support_agent.name}</td>
                                    <td className={ticket.status.value + " status ft-xs-hide"}>
                                        {ticket.status.label}
                                    </td>
                                    <td>
                                        <div className="btn-group btn-group-xs" role="group">
                                            <button className="ft-view-ticket btn btn-success"
                                                    onClick={this.viewTicketById(ticket.id)}>
                                                View
                                            </button>
                                        </div>
                                    </td>
                                </tr>)
                            }
                        </tbody>
                        </table>
                    }
                </div>
            </div>
        )
    }
});
