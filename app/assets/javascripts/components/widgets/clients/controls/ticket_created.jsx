var ClientTicketItemCreated = React.createClass({
    render() {
        return (
            <div className="col-md-12">
                <h1 id="ft-ticket-title">
                    Your ticket has been submitted successfully!
                </h1>

                <NavLinks links={ [
                    {onClick: this.props.viewTickets, text: 'Back To My Tickets'},
                    {onClick: this.props.createTicket, text: 'Submit a new Ticket'},
                ]}/>
            </div>
        )
    }
});
