var ClientTicketItemCreate = React.createClass({
    getInitialState() {
        return { categories: Store.Categories.getAll() }
    },

    updateCategories(){
        this.setState({ categories: Store.Categories.getAll() })
    },

    componentDidMount() {
        Store.Categories.bind('categories.loaded', this.updateCategories);
        Store.Categories.loadAll();
    },

    componentWillUnmount: function() {
        Store.Categories.unbind('categories.loaded', this.updateCategories);
    },

    handleSubmit(event){
        event.preventDefault();
        const ticket = {
            title: this.state.title,
            category: this.state.category,
            description: this.state.description
        };

        Store.ClientTickets.createTicket(ticket);
        this.props.createdTicket();
    },

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.id;

        this.setState({
            [name]: value
        });
    },

    submitEnabled(){
      return this.state.title && this.state.category && this.state.description
    },

    render() {
        return (
            <div className="col-md-3">
                <NavLinks links={ [
                    {onClick: this.props.viewTickets, text: 'Back To My Tickets'},
                ]}/>

                <h1 id="ft-ticket-title">
                    Submit a Ticket
                </h1>
                {
                    !this.state.categories &&
                    <Loading/>
                }
                {
                    this.state.categories &&
                    <form onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <label htmlFor="title">Problem</label>
                            <input type="title" className="form-control" id="title"
                                   onChange={this.handleInputChange}
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="category">Category</label>
                            <select type="category" className="form-control" id="category"
                                    onChange={this.handleInputChange}
                            >
                                <option value="">Please Choose</option>
                                {
                                    this.state.categories.map((category) =>
                                        <option key={category.name} value={category.name}>{category.name}</option>
                                    )
                                }
                            </select>
                        </div>

                        <div className="form-group">
                            <label htmlFor="description">Description</label>
                            <textarea className="form-control" id="description"
                                      onChange={this.handleInputChange}/>
                        </div>

                        <button type="submit" className="btn btn-success"
                                title={this.submitEnabled() ? "" : "Please fill in all fields"}
                                disabled={!this.submitEnabled()}>Submit</button>
                    </form>

                }
            </div>
        )}
});
