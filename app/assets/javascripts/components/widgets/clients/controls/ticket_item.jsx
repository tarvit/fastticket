var ClientTicketItem = React.createClass({
    getInitialState() {
        return {
            ticket: Store.ClientTickets.getTicket(this.props.ticketId),
            ticketId: this.props.ticketId
        }
    },

    componentDidMount() {
        Store.ClientTickets.loadTicket(this.props.ticketId);
        Store.ClientTickets.bind('ticket.'+this.props.ticketId+'.loaded', this.updateTicket);
    },

    componentWillUnmount: function() {
        Store.ClientTickets.unbind('ticket.'+this.props.ticketId+'.loaded', this.updateTicket);
    },

    processingStatus(){
        return (this.state.ticket && this.state.ticket.status.value == 'processing')
    },

    newStatus(){
        return (this.state.ticket && this.state.ticket.status.value == 'new')
    },

    updateTicket(){
        this.setState({ ticket: Store.ClientTickets.getTicket(this.props.ticketId) });
    },

    render() {
        return (
            <div>
                <div className="col-md-12">
                    <NavLinks links={ [
                        {onClick: this.props.viewTickets, text: 'Back To My Tickets'},
                        {onClick: this.props.createTicket, text: 'Submit a Ticket'},
                    ]}/>

                    <h1 id="ft-ticket-title">
                        Ticket#
                        <span>{this.state.ticketId}</span>
                    </h1>
                    <hr/>
                </div>
                <div className="col-md-5">
                    {!this.state.ticket && <Loading/> }
                    {this.state.ticket &&
                        <TicketDetails ticket={this.state.ticket}/>
                    }
                </div>

                <div className="col-md-5">
                    <Conversation ticketId={this.state.ticketId}
                                  postingEnabled={this.newStatus() || this.processingStatus()}
                                  conversationsStore={Store.ClientConversations}/>
                </div>
            </div>
        )}
});
